#!/bin/bash

function force_location()
{
    current_dir=$(dirname $0);

    if [ ! -f ${current_dir}/tests/vagrant/vagrant-template/Vagrantfile ]; then
            echo "Aborting."
            echo
            echo "The tests/vagrant subdirectory seems to be missing."
            echo
            echo "Please correct the problem and try again."
            echo
            exit 1
        fi
}


force_location

echo "Testing for Vagrant.... (TODO)"
echo "Testing for Ansible.... (TODO)"

echo "Copy vagrant-template dir to a new workdir based on git branch"
BRANCHNAME=`git rev-parse --abbrev-ref HEAD`
echo $BRANCHNAME
mkdir -p tests/vagrant/$BRANCHNAME
cp -R tests/vagrant/vagrant-template/* tests/vagrant/$BRANCHNAME

echo "Creating tar of source...."
mkdir -p tests/vagrant/$BRANCHNAME/roles/compile-gluster/files
tar chf tests/vagrant/$BRANCHNAME/roles/compile-gluster/files/glusterfs-vagrant-test.tar.gz  --exclude=*glusterfs-vagrant-test.tar.gz .

echo "change dir to new vagrant dir"
cd tests/vagrant/$BRANCHNAME

vagrant up --provision
