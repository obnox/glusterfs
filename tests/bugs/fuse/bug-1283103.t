#!/bin/bash

. $(dirname $0)/../../include.rc
#. $(dirname $0)/../../volume.rc

cleanup;

#Basic checks
TEST glusterd
TEST pidof glusterd
TEST $CLI volume info

#Create a distributed volume
TEST $CLI volume create $V0 $H0:$B0/${V0}{1..2};
TEST $CLI volume start $V0

# Mount FUSE without selinux:
TEST glusterfs -s $H0 --volfile-id $V0 $M0

TESTFILE="$M0/testfile"
TEST touch ${TESTFILE}

TEST echo "setfattr -n security.foobar -v value ${TESTFILE}"
TEST setfattr -n security.foobar -v value ${TESTFILE}
TEST ! getfattr -n security.selinux ${TESTFILE}
TEST ! setfattr -n security.selinux -v value ${TESTFILE}

TEST umount $M0

# Mount FUSE without selinux:
TEST glusterfs -s $H0 --volfile-id $V0 --selinux $M0

TEST setfattr -n security.foobar -v value ${TESTFILE}
TEST setfattr -n security.selinux -v value ${TESTFILE}
TEST getfattr -n security.selinux ${TESTFILE}
TEST setfattr -x security.selinux ${TESTFILE}


# EXPECT_WITHIN $UMOUNT_TIMEOUT "Y" force_umount $M0
# TEST $CLI volume stop $V0
# TEST $CLI volume delete $V0;
# TEST ! $CLI volume info $V0;

cleanup;
